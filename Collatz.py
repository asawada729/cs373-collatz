#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

from typing import IO, List

cache = dict()
# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """

    # Make sure i is lower bound and j is upper bound
    if i > j:
        c = j
        j = i
        i = c

    assert i > 0
    assert j >= i

    # Rule we covered in class so that we only have to cover half of the range
    m = j//2 + 1
    if i < m:
        i = m
    mx = 0
    s = i
    while s <= j:
        tmp = s
        cnt = 1
        while tmp > 1:

            # Check if cycle length for a number is already in cache
            if tmp in cache:
                cache[s] = cache[tmp] + cnt - 1
                break

            # If not, compute one cycle
            if tmp % 2 == 0:
                tmp = tmp//2
                cnt += 1

            else:
                tmp = 3*tmp + 1
                tmp = tmp//2
                cnt += 2

        # This is for when no cycle length was found in cache.
        # We simply assign a new index.
        else:
            assert cnt > 0
            cache[s] = cnt
        if cache[s] > mx:
            mx = cache[s]
        s += 1

    return mx

# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
